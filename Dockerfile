# pull the official base image
FROM python:3.10.6-alpine

# set work directory
WORKDIR /usr/src/app

# set environment variables
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip 
COPY ./requirements.txt /usr/src/app
RUN pip install -r requirements.txt

# copy project
COPY . /usr/src/app

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]




# Use an official Python runtime as a parent image
#FROM python:3.9-slim-buster

# Set environment variables
#ENV PYTHONDONTWRITEBYTECODE 1
#ENV PYTHONUNBUFFERED 1

# Set working directory
#WORKDIR /code

# Copy the requirements file
#COPY requirements.txt .

# Install any needed packages specified in requirements.txt
#RUN pip install --no-cache-dir -r requirements.txt

# Copy the project code into the container
#COPY . .

# Start Gunicorn
#CMD ["gunicorn", "--bind", "0.0.0.0:8000", "myproject.wsgi:application"]

